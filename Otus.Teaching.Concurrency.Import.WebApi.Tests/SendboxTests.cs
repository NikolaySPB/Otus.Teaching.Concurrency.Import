using System;
using Xunit;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Tests
{
    public class SendboxTests
    {
        [Theory]
        [InlineData(414, "Clarabelle Cartwright", "ClarabelleCartwright72@gmail.com", "1-736-562-0178")]
        [InlineData(715, "Walker Kshlerin", "WalkerKshlerin_Price45@yahoo.com", "1-194-056-4549")]
        [InlineData(263, "Rashad Thompson", "RashadThompson98@hotmail.com", "1-645-384-6133")]
        [InlineData(1127457275, "Joannie Reichel", "JoannieReichel_Hodkiewicz@gmail.com", "1-564-811-2277")]
        public async Task GetCustomerByIdAsync_Test(int expId, string expFullName, string expEmail, string expPhone)
        {
            // arrange
            var sb = new Sandbox();

            // act
            var customer = await sb.GetCustomerByIdAsync(expId);

            // assert
            Assert.True(customer.Id == expId);
            Assert.Equal(customer.FullName, expFullName);
            Assert.Equal(customer.Email, expEmail);
            Assert.Equal(customer.Phone, expPhone);
        }

        [Theory]
        [InlineData(890, "Cris Pavlovich", "cris_pavlovich@mail.ru", "8-950-500-01-01")]
        public async Task AddCustomerAsyncs_Test(int expId, string expFullName, string expEmail, string expPhone)
        {
            // arrange
            var sb = new Sandbox();
            var customer = new Customer 
            { 
                Email = expEmail, 
                FullName = expFullName, 
                Id = expId, 
                Phone = expPhone 
            };

            // act
            await sb.AddCustomerAsync(customer);
            var resCust = sb.GetCustomerByIdAsync(expId);

            // assert
            Assert.NotNull(resCust);
            Assert.Equal(customer.Id, expId);
            Assert.Equal(customer.FullName, expFullName);
            Assert.Equal(customer.Email, expEmail);
            Assert.Equal(customer.Phone, expPhone);
        }

        [Fact]
        public async Task GetAllCustomersAsync_Test()
        {
            // arrange
            var sb = new Sandbox();

            // act
            var customers = await sb.GetAllCustomersAsync();

            // assert
            Assert.NotNull(customers);
            Assert.True(customers.Count != 0);            
        }
    }
}
