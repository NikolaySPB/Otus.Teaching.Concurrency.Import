﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi
{
    public class Sandbox
    {
        private readonly HttpClient client;
        private readonly string sandboxURL = "https://winter-wave-3753.getsandbox.com:443";

        public Sandbox()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(sandboxURL)
            };
        }

        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            var response = await client.GetAsync($"/users/{id}");
            var result = await response.Content.ReadAsStringAsync();

            if ((int)response.StatusCode != 404)
            {
                var customer = JsonConvert.DeserializeObject<Customer>(result);
                return customer;
            }
            else
            {
                return null;
            }
        }

        public async Task<string> AddCustomerAsync(Customer customer)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customer));
            var response = await client.PostAsync("/users", content);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            var response = await client.GetAsync("/users");
            var result = await response.Content.ReadAsStringAsync();
            var customers = JsonConvert.DeserializeObject<List<Customer>>(result);
            return customers;
        }
    }
}
