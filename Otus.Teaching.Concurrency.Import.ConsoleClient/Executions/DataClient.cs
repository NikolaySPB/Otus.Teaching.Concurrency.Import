﻿using Otus.Teaching.Concurrency.Import.ConsoleClient.Interfaces;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebApi;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient.Executions
{
    class DataClient : IDataClient
    {
        private readonly IDataConsole _dataConsole;
        private readonly Sandbox _sandbox;

        public DataClient(Sandbox sandbox, IDataConsole dataConsole)
        {
            _dataConsole = dataConsole;
            _sandbox = sandbox;
        }

        public void GetCommands()
        {
            _dataConsole.WriteLine("\nCommands for Web Api:");
            _dataConsole.WriteLine("add - Add a random customer generation and send it to the server for saving");
            _dataConsole.WriteLine("get id - Get customer by ID");
            _dataConsole.WriteLine("get all - Get all customers on the server");

            var selectedTeam = _dataConsole.GetInput("Enter the command: ");
            ProcessCommand(selectedTeam).GetAwaiter().GetResult();
        }

        private async Task ProcessCommand(string selectedTeam)
        {
            _dataConsole.Clear();
            switch (selectedTeam)
            {
                case "add":
                    await AddCustomerAsync();
                    break;
                case "get id":
                    await GetCustomerByIdAsync();
                    break;
                case "get all":
                    await GetAllCustomersAsync();
                    break;
                default:
                    _dataConsole.WriteLine("This command is missing or entered incorrectly.");
                    break;
            }
            GetCommands();
        }

        public async Task AddCustomerAsync()
        {
            var customer = RandomCustomerGenerator.CreateOneRandomFaker();

            var result = await _sandbox.AddCustomerAsync(customer);

            _dataConsole.WriteLine(result);
        }

        public async Task<List<Customer>> GetAllCustomersAsync()
        {
            List<Customer> customers;
            try
            {
                customers = await _sandbox.GetAllCustomersAsync();
            }
            catch(HttpRequestException ex)
            {
                _dataConsole.WriteLine(ex.Message);
                return null;
            }

            _dataConsole.WriteLine("Customers found:");            

            foreach (var customer in customers)
            {
                _dataConsole.WriteLine(customer.ToString());
            }

            return customers;
        }

        public async Task<Customer> GetCustomerByIdAsync()
        {
            var id = Convert.ToInt32(_dataConsole.GetInput("Input customer id: "));

            Customer customer;

            try
            {
                customer = await _sandbox.GetCustomerByIdAsync(id);
            }
            catch (HttpRequestException ex)
            {
                _dataConsole.WriteLine(ex.Message);
                return null;
            }

            _dataConsole.WriteLine("Customer found:");            
            
            _dataConsole.WriteLine(customer.ToString());

            return customer;
        }
    }
}
