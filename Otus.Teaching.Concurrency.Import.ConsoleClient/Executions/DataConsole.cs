﻿using Otus.Teaching.Concurrency.Import.ConsoleClient.Interfaces;
using System;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient.Executions
{
    class DataConsole : IDataConsole
    {
        public void Clear()
        {
            Console.Clear();
        }                

        public string GetInput(string text)
        {
            Write(text);
            var data = Console.ReadLine();
            return data;
        }

        public void Write(string text)
        {
            Console.Write(text);
        }

        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}
