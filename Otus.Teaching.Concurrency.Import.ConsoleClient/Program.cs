﻿using Otus.Teaching.Concurrency.Import.ConsoleClient.Executions;
using Otus.Teaching.Concurrency.Import.WebApi;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Sandbox sandbox = new Sandbox();

            DataConsole dataConsole = new DataConsole();

            DataClient dataClient = new DataClient(sandbox, dataConsole);

            dataClient.GetCommands();
        }
    }
}
