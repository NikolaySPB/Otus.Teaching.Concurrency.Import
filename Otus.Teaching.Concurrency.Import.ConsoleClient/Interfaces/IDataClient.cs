﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleClient.Interfaces
{
    interface IDataClient
    {
        Task AddCustomerAsync();
        Task<Customer> GetCustomerByIdAsync();
        Task<List<Customer>> GetAllCustomersAsync();
    }
}
