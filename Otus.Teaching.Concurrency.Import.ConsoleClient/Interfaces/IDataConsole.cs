﻿namespace Otus.Teaching.Concurrency.Import.ConsoleClient.Interfaces
{
    interface IDataConsole
    {
        string GetInput(string text);
        void WriteLine(string text);
        void Write(string text);
        void Clear();
    }
}
