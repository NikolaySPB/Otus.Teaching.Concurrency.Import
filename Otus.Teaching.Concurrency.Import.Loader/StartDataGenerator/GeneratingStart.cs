﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class GeneratingStart
    {
        public static int _dataCount;

        private const string ProcessFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";

        private const string ProcessDirectory = @"D:\Homework\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1";

        private string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        internal Selection Selection = Selection.Method;

        private Process process;

        public void GenerateCustomersDataFile(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            if (Selection == Selection.Method)
            {
                var xmlGenerator = new XmlGenerator(_dataFilePath, _dataCount);

                xmlGenerator.Generate();
            }
            else
            {
                GenerateCustomersDataFileProcess();

                process.WaitForExit();

                process.Kill();
            }
        }

        public async void GenerateCustomersDataFileAsync(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            if (Selection == Selection.Method)
            {
                var xmlGenerator = new XmlGenerator(_dataFilePath, _dataCount);

                await Task.Run(() => xmlGenerator.Generate());
            }
            else
            {
                await Task.Run(() => GenerateCustomersDataFileProcess());

                process.WaitForExit();

                process.Kill();
            }
        }

        private Process GenerateCustomersDataFileProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                Arguments = _dataFilePath,

                FileName = GetPathToHandlerProcess()
            };

            process = Process.Start(startInfo);

            return process;
        }

        private static string GetPathToHandlerProcess()
        {
            return Path.Combine(ProcessDirectory, ProcessFileName);
        }
    }
}
