﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.DataBase;
using System;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Program
    {
        static void Main(string[] args)
        {
            GeneratingStart._dataCount = 1000000;

            var selectionSQL = SelectionSQL.SQLite;

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GeneratingStart generatingStart = new GeneratingStart();

            /* В классе GeneratingStart по умолчанию присвоено Selection = Selection.Method, т.е. генератор запускается методом. 
             * Для запуска Процесса-генератора файла, необходимо снять комментарий со строки кода: "generatingStart.Selection = Selection.Process;"
             * Так же для первого запуска Процесса-генератора файла необходимо собрать проект Otus.Teaching.Concurrency.Import.DataGenerator.App
             * Сейчас в проекте Процесса-генератора файла по умолчанию задано на 1 млн. генераций Customer - объектов*/

            //generatingStart.Selection = Selection.Process;

            generatingStart.GenerateCustomersDataFileAsync(args);

            InitializeDataBase(selectionSQL);

            var loader = new DataLoader();

            int numberOfThreads = Environment.ProcessorCount;

            loader.LoadData(numberOfThreads, selectionSQL);

            Console.ReadLine();
        }

        static void InitializeDataBase(SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
            {
                using var mySQLServerContext = new MySQLServerContext();

                new InitializerSQL(selectionSQL, mySQLServerContext).InitializeSQL();
            }
            else
            {
                using var mySQLiteContext = new MySQLiteContext();

                new InitializerSQL(selectionSQL, mySQLiteContext).InitializeSQL();
            }

        }
    }
}