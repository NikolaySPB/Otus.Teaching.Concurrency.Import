﻿using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class Accelerator
    {
        private SelectionSQL _selectionSQL;

        public const int PortionSize = 5000;

        private readonly IEnumerable<Customer> _customers;

        public Accelerator(IEnumerable<Customer> customers, SelectionSQL selectionSQL)
        {
            _customers = customers;

            _selectionSQL = selectionSQL;
        }

        public void SortOut()
        {
            var portions = _customers.Portion(PortionSize);

            foreach (var portion in portions)
            {
                using var repository = new CustomerRepository(_selectionSQL);

                Task.Run(() => repository.AddCustomersAsync(portion, _selectionSQL)).Wait();

                AttemptsToSave(repository);
            }
        }

        private void AttemptsToSave(CustomerRepository repository)
        {
            int Counter = 0;
            do
            {
                try
                {
                    Task.Run(()=> repository.SaveAsync(_selectionSQL)).Wait();

                    break;
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc);

                    Counter++;
                }
            }
            while (Counter < 50);
        }
    }
}
