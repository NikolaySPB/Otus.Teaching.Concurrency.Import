﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private const string path = @"D:\Homework\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\netcoreapp3.1\customers.xml";
        public List<Customer> Parse()
        {
            using var stream = new StreamReader(path);

            var customersList = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(stream);

            return customersList.Customers;
        }

        public Task<List<Customer>> ParseAsync()
        {
            var task = new Task<List<Customer>>(Parse);
            task.Start();
            return task;
        }
    }
}