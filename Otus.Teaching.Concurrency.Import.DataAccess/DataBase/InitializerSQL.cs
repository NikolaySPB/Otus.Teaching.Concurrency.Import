﻿using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DataBase
{
    public class InitializerSQL
    {
        private readonly MySQLServerContext _mySQLServerContext;

        private readonly MySQLiteContext _mySQLiteContext;

        private SelectionSQL _selectionSQL;

        public InitializerSQL(SelectionSQL selectionSQL, object dataContext)
        {
            _selectionSQL = selectionSQL;

            if (selectionSQL == SelectionSQL.SQLServer)
                _mySQLServerContext = (MySQLServerContext)dataContext;
            else
                _mySQLiteContext = (MySQLiteContext)dataContext;
        }

        public void InitializeSQL()
        {
            if (_selectionSQL == SelectionSQL.SQLServer)
            {
                _mySQLServerContext.Database.EnsureDeleted();

                _mySQLServerContext.Database.EnsureCreated();
            }
            else
            {
                _mySQLiteContext.Database.EnsureDeleted();

                _mySQLiteContext.Database.EnsureCreated();
            }
        }
    }
}
