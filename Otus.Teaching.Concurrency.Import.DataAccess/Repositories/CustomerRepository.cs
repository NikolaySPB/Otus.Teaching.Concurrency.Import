using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private MySQLServerContext mySQLServerContext;

        private MySQLiteContext mySQLiteContext;

        private bool disposed = false;

        public CustomerRepository(SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                mySQLServerContext = new MySQLServerContext();
            else
                mySQLiteContext = new MySQLiteContext();
        }

        public void AddCustomer(Customer customer, SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                mySQLServerContext.Customers.Add(customer);
            else
                mySQLiteContext.Customers.Add(customer);
        }

        public void AddCustomers(IEnumerable<Customer> customers, SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                mySQLServerContext.Customers.AddRange(customers);
            else
                mySQLiteContext.Customers.AddRange(customers);
        }

        public void Save(SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                mySQLServerContext.SaveChanges();
            else
                mySQLiteContext.SaveChanges();
        }

        public async Task AddCustomerAsync(Customer customer, SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                await mySQLServerContext.Customers.AddAsync(customer);
            else
                await mySQLiteContext.Customers.AddAsync(customer);
        }

        public async Task AddCustomersAsync(IEnumerable<Customer> customers, SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                await mySQLServerContext.Customers.AddRangeAsync(customers);
            else
                await mySQLiteContext.Customers.AddRangeAsync(customers);
        }

        public async Task SaveAsync(SelectionSQL selectionSQL)
        {
            if (selectionSQL == SelectionSQL.SQLServer)
                await mySQLServerContext.SaveChangesAsync();
            else
                await mySQLiteContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                mySQLServerContext?.Dispose();

                mySQLiteContext?.Dispose();
            }

            disposed = true;
        }
    }
}